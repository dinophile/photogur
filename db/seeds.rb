# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Picture.create!(
	:title  => "Two Cats One (Set of) Stairs",
	:artist => "Cheyenne",
	:url    => "http://i.imgur.com/d9xbTYR.jpg"
)

Picture.create!(
	:title  => "Queen Bee and Her Minions",
	:artist => "Also Cheyenne",
	:url    => "http://i.imgur.com/e98FAqH.jpg"
)

Picture.create!(
	:title  => "Princess SparklePony",
	:artist => "Cheyenne Again",
	:url    => "http://i.imgur.com/yIGFXA0.jpg"
)
